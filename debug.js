/* global F CONF */

'use strict';
require('make-promises-safe');

// ===================================================
// FOR DEVELOPMENT
// Total.js - framework for Node.js platform
// https://www.totaljs.com
// ===================================================

const options = {};

// options.ip = '127.0.0.1';
// options.port = parseInt(process.argv[2]);
// options.config = { name: 'Total.js' };
// options.sleep = 3000;
// options.inspector = 9229;
// options.watch = ['private'];

// require('total.js/debug')(options);
require('total.js');

const { v4: uuidv4 } = require('uuid');
const socketio = require('socket.io');
const revalidator = require('revalidator');
const ChunkHandler = require('chunk-handler');
const chat = require(F.path.definitions('chatlib'));
const helper = require(F.path.definitions('helper'));
const dbMemory = require(F.path.definitions('userinmemory'));

const dbUserOnline = [];
const chunk = new ChunkHandler();

F.on('load', async function () {
  this.io = socketio(this.server);

  this.io.on('connection', async function (socket) {
    if (CONF.ws_debug) console.log('Client id: ' + socket.id + ' is connected');
    // add user in db memory
    dbMemory.addUser(dbUserOnline, socket.id);

    socket.on('join', async function (data) {
      const validator = revalidator.validate(data, chat.joinSchema);
      if (validator.valid) {
        if (CONF.ws_debug) console.log(data.account_id + ': is joining chat room: ', data.room_id);
        socket.join(data.room_id);
        // send emit to joined event
        chat.joinRoom(data, socket);
        //   ws.validateConsult(data.room_id, data.account_id, data.account_type_id, socket);

        // modify db user online
        const isonline = dbMemory.updateUser(dbUserOnline, socket.id, data.account_id, data.room_id);
        if (isonline) {
          socket.emit('isOnline', chat.sendResponse('true', 'User is online.', '', isonline));
          if (CONF.ws_debug) console.log(data.account_id + ': is online in room id: ', data.room_id);
        }
      } else {
        if (CONF.ws_debug) console.log(JSON.stringify(validator.errors));
        socket.emit('join', chat.sendResponse('false', 'Ada kesalahan... ', 'error', validator.errors));
      }
    });

    socket.on('loadHistory', async function (data) {
      const validator = revalidator.validate(data, chat.loadhistorySchema);
      if (validator.valid) {
        const loadHistory = await chat.loadMessages(data.room_id, socket);
        socket.emit('loadHistory', loadHistory);
      } else {
        if (CONF.ws_debug) console.log(JSON.stringify(validator.errors));
        socket.emit('loadHistory', chat.sendResponse('false', 'Ada kesalahan...', 'error', validator.errors));
      }
    });

    socket.on('message', async function (data) {
      const date = new Date();
      const sendData = {
        room_id: data.room_id,
        account_id: data.account_id,
        account_type_id: data.account_type_id,
        message_type_id: data.message_type_id,
        message: data.message
      };

      const validator = revalidator.validate(sendData, chat.messageSchema);
      if (validator.valid) {
        sendData.message_id = uuidv4();
        sendData.date_created = helper.timestamp(date);
        sendData.date_created_unix = date.getTime();
        sendData.message_status_id = 1;
        sendData.status_active_id = 1;
        sendData.date_modified = '';
        sendData.date_modified_unix = '';

        switch (sendData.message_type_id) {
          case 1: // case text
            // no parse for text
            break;
          default:
            if (sendData.message.indexOf(';base64,') > 0) {
              const parse = helper.base64FileHandler(sendData.message, uuidv4());
              if (parse) {
                sendData.message = parse;
              } else {
                sendData.message = '';
              }
            } else {
              // correction type message
              sendData.message_type_id = 1;
            }
        }
        // write logic to saving message to database with message status received in database
        if (socket.rooms.has(data.room_id)) {
          // make sure text message is exist
          if (sendData.message) {
            const resData = await chat.insertMessage(sendData);
            if (chat.hasKey(data, ['nickname'])) {
              resData.nickname = data.nickname;
              sendData.nickname = data.nickname;
            }
            if (CONF.ws_debug) console.log('Sending ', sendData);
            socket.broadcast.to(data.room_id).emit('message', resData);
            socket.broadcast.emit('newmessage', chat.sendResponse('true', 'Ada pesan baru', '', sendData));
            if (chat.hasKey(data, ['element_id'])) {
              sendData.element_id = data.element_id;
            }
            socket.emit('delivered', chat.sendResponse('true', 'Pesan Anda berhasil terkirim', '', sendData));
          } else {
            socket.emit('message', chat.sendResponse('false', 'Pesan Anda gagal terkirim.', '', ''));
            if (CONF.ws_debug) console.log(data.account_id + ': tidak dapat mengirim karena file corrupt');
          }
        } else {
          if (CONF.ws_debug) console.log(data.account_id + ': tidak dapat mengirim chat karena room telah nonaktif');
        }
      } else {
        if (CONF.ws_debug) console.log(JSON.stringify(validator.errors));
        socket.emit('message', JSON.parse(chat.sendResponse('false', 'Ada kesalahan... ', 'error', validator.errors)));
      }
    });

    socket.on('read', async function (data) {
      const validator = revalidator.validate(data, chat.readSchema);
      if (validator.valid) {
        const isRead = await chat.messageIsRead(data);
        if (isRead) {
          socket.broadcast.to(data.room_id).emit('read', isRead);
        } else {
          socket.emit('read', chat.sendResponse('false', 'Ada kesalahan...', 'error', 'Something went wrong!'));
        }
      } else {
        if (CONF.ws_debug) console.log('read: ' + JSON.stringify(validator.errors));
        socket.emit('read', chat.sendResponse('false', 'Ada kesalahan... ', 'error', validator.errors));
      }
    });

    socket.on('delete', async function (data) {
      const validator = revalidator.validate(data, chat.deleteSchema);
      if (validator.valid) {
        const isDeleted = await chat.messageIsDeleted(data);
        if (isDeleted) {
          socket.broadcast.to(data.room_id).emit('delete', isDeleted);
          socket.emit('delete', isDeleted);
        } else {
          socket.emit('delete', chat.sendResponse('false', 'Ada kesalahan...', 'error', 'Something went wrong!'));
        }
      } else {
        if (CONF.ws_debug) console.log('delete: ' + JSON.stringify(validator.errors));
        socket.emit('delete', chat.sendResponse('false', 'Ada kesalahan...', 'error', validator.errors));
      }
    });

    socket.on('broadcast', function (data) {
      chat.broadcastMessage(data, socket);
    });

    socket.on('typing', function (data) {
      const validator = revalidator.validate(data, chat.typingSchema);
      if (validator.valid) {
        chat.messageIsTyping(data, socket);
      } else {
        if (CONF.ws_debug) console.log('typing: ' + JSON.stringify(validator.errors));
        socket.emit('typing', chat.sendResponse('false', 'Ada kesalahan...', 'error', validator.errors));
      }
    });

    socket.on('isOnline', function (data) {
      let checkuseronline;
      if (data.room_id) {
        checkuseronline = dbMemory.isUserOnline(dbUserOnline, data.account_id, data.room_id);
      } else {
        checkuseronline = dbMemory.isUserOnline(dbUserOnline, data.account_id);
      }
      if (checkuseronline) {
        socket.emit('isOnline', chat.sendResponse('true', 'User is online.', '', checkuseronline));
      } else {
        data.status = 'offline';
        socket.emit('isOnline', chat.sendResponse('true', 'User is offline.', '', data));
      }
    });

    socket.on('disconnect', function (data) {
      // send user is offline
      const isoffline = dbMemory.logoutUser(dbUserOnline, socket.id);
      socket.broadcast.to(data.room_id).emit('isOnline', chat.sendResponse('true', 'User is offline.', '', isoffline));
      if (CONF.ws_debug) console.log('Client ' + socket.id + ' is disconnected');
    });

    socket.on('chunkOpen', function (data) {
      socket.emit('chunkReady', chat.sendResponse('true', 'Chunk ' + data.name + ' is ready to send.', '', ''));
      setTimeout(function () {
        let string = chunk.get(data.name);
        if (!chunk.isEmptyArray(string)) {
          chunk.remove(data.name);
          socket.emit('chunkTimeout', chat.sendResponse('false', 'Sending chunk ' + data.name + ' is timeout.', '', ''));
          string = null;
        }
      }, F.config.ws_chunk_timeout);
    });

    socket.on('chunkSend', function (data) {
      chunk.add(data.name, data.chunk, data.part);
      let string = chunk.get(data.name);
      if (!chunk.isEmptyArray(string)) {
        if (data.length === string.length) {
          const merge = chunk.merge(string);
          const parse = helper.base64FileHandler(merge, uuidv4());
          if (parse) {
            socket.emit('chunkComplete', chat.sendResponse('true', 'Chunk ' + data.name + ' is finish received.', '', parse));
            // if there is message object then will send message after chunk is completed
            if (!chunk.isEmpty(data.message) && !chunk.isEmptyObject(data.message)) {
              // Automatically send message
              const sendData = {
                room_id: data.message.room_id,
                account_id: data.message.account_id,
                account_type_id: data.message.account_type_id,
                message_type_id: data.message.message_type_id,
                message: parse
              };

              const validator = revalidator.validate(sendData, chat.messageSchema);
              if (validator.valid) {
                const date = new Date();
                sendData.message_id = uuidv4();
                sendData.date_created = helper.timestamp(date);
                sendData.date_created_unix = date.getTime();
                sendData.message_status_id = 1;
                sendData.status_active_id = 1;
                sendData.date_modified = '';
                sendData.date_modified_unix = '';

                // write logic to saving message to database with message status received in database
                if (socket.rooms.has(data.message.room_id)) {
                  // make sure text message is exist
                  if (sendData.message) {
                    const resData = chat.insertMessage(sendData);
                    // notiffcm(userOnline, sendData);
                    if (chat.hasKey(data.message, ['nickname'])) {
                      resData.nickname = data.message.nickname;
                      sendData.nickname = data.message.nickname;
                    }
                    if (CONF.ws_debug) console.log('Sending ', sendData);
                    socket.broadcast.to(data.message.room_id).emit('message', resData);
                    socket.broadcast.emit('newmessage', chat.sendResponse('true', 'Ada pesan baru', '', sendData));
                    if (chat.hasKey(data.message, ['element_id'])) {
                      sendData.element_id = data.message.element_id;
                    }
                    socket.emit('delivered', chat.sendResponse('true', 'Pesan Anda berhasil terkirim', '', sendData));
                  } else {
                    socket.emit('message', chat.sendResponse('false', 'Pesan Anda gagal terkirim.', '', ''));
                  }
                } else {
                  if (CONF.ws_debug) console.log(data.message.account_id + ': tidak dapat mengirim chat karena room telah nonaktif');
                }
              } else {
                if (CONF.ws_debug) console.log('chunk: ' + JSON.stringify(validator.errors));
                socket.emit('message', chat.sendResponse('false', 'Ada kesalahan...', 'error', validator.errors));
              }
            }
          } else {
            socket.emit('chunkError', chat.sendResponse('true', 'Sending chunk ' + data.name + ' is corrupted.', '', ''));
          }
          chunk.remove(data.name);
        }
      }
      string = null;
    });
  });
});

F.http('debug', options);
