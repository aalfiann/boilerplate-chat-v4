/**
 * Schema Message for validate the socket request
 * @return {object}
 */
const messageSchema = {
  type: 'object',
  required: true,
  properties: {
    room_id: {
      type: 'string',
      required: true
    },
    account_id: {
      type: 'string',
      required: true
    },
    account_type_id: {
      type: 'number',
      required: true
    },
    message_type_id: {
      type: 'number',
      required: true
    },
    message: {
      type: 'string',
      required: true
    }
  }
};

/**
 * Schema Join for validate the socket request
 */
const joinSchema = {
  type: 'object',
  required: true,
  properties: {
    room_id: {
      type: 'string',
      required: true
    },
    account_id: {
      type: 'string',
      required: true
    },
    account_type_id: {
      type: 'number',
      required: true
    }
  }
};

/**
 * Schema Read for validate the socket request
 */
const readSchema = {
  type: 'object',
  required: true,
  properties: {
    room_id: {
      type: 'string',
      required: true
    },
    message_id: {
      type: 'string',
      required: true
    }
  }
};

/**
 * Schema Delete for validate the socket request
 */
const deleteSchema = {
  type: 'object',
  required: true,
  properties: {
    room_id: {
      type: 'string',
      required: true
    },
    message_id: {
      type: 'string',
      required: true
    },
    account_id: {
      type: 'string',
      required: true
    }
  }
};

/**
 * Schema Typing for validate the socket request
 */
const typingSchema = {
  type: 'object',
  required: true,
  properties: {
    room_id: {
      type: 'string',
      required: true
    },
    account_id: {
      type: 'string',
      required: true
    }
  }
};

/**
 * Schema Load History for validate the socket request
 */
const loadhistorySchema = {
  type: 'object',
  required: true,
  properties: {
    room_id: {
      type: 'string',
      required: true
    }
  }
};

module.exports = {
  messageSchema,
  joinSchema,
  readSchema,
  deleteSchema,
  typingSchema,
  loadhistorySchema
};
